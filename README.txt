Installation instructions
=========================

1. Download the CardEaseXML SDK PHP library from
   http://www.creditcall.co.uk/dev_cardeasexml.shtml

2. Unpack the downloaded file into the uc_creditcall directory, so that
   uc_creditcall/CardEaseXMLClientPHP/src/Client.php can be found.

3. Enable the module.

4. Configure the module at /admin/store/settings/payment/edit/gateways

